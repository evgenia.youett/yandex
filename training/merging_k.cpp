#include <iostream>
#include <queue>
#include <vector>
#include <map>

using std::cin;
using std::cout;

int main()
{
    int k;
    cin >> k;

    std::vector<std::queue<int16_t>> temp(k);

    int size = 0;

    for(int i = 0; i < k; i++)
    {
        int n;
        cin >> n;

        for(int j = 0; j < n; j++)
        {
            int16_t number;
            cin >> number;
            temp[i].push(number);
        }

        size += n;

    }

    std::multimap<int16_t, int> beginnings;

    for(int i = 0; i < k; i++)
    {
        if(!temp[i].empty())
        {
            beginnings.insert(std::make_pair(temp[i].front(),i));
            temp[i].pop();
        }
    }

    for(int i = 0; i < size; i++)
    {
        int num_rem = beginnings.begin()->second;
        cout << beginnings.begin()->first << " ";
        beginnings.erase(beginnings.begin());
        if(!temp[num_rem].empty())
        {
            beginnings.insert(std::make_pair(temp[num_rem].front(), num_rem));
            temp[num_rem].pop();
        }

    }

}
