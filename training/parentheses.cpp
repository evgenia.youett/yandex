#include <stdio.h>
#include <iostream>
#include <set>
#include <string>

const int size_max = 11;

using std::cin;
using std::cout;
using std::string;
using std::set;

void buildParenthesis(set<string>& output, int pos, int n, int open, int close)
{
    static char str[size_max];

    if(close == n)
    {
        output.insert(string(str));
        return;
    }
    else
    {
        if(open > close)
        {
            str[pos] = ')';
            buildParenthesis(output, pos+1, n, open, close+1);
        }

        if(open < n)
        {
            str[pos] = '(';
            buildParenthesis(output, pos+1, n, open+1, close);
        }
    }
}

int main()
{
    int n;
    cin >> n;

    set<string> result;

    buildParenthesis(result, 0, n, 0, 0);

    for(auto& el: result)
        cout << el << "\n";

    return 0;
}
