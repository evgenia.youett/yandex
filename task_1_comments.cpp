#include <stdio.h>

class Feature
{
public:
    enum FeatureType {eUnknown, eCircle, eTriangle, eSquare};
    // enum class is in general better but requires casting or overloading for reading from file

    Feature() : type(eUnknown), points(0) { }

    ~Feature()
    // default copy and move constructors and assignment operators are deleted
    // and have to be implemented if they are needed
    {
        if (points)
            delete points; // should be delete[] points
    }

    bool isValid() // should be const
    {
        return type != eUnknown;
    }

    bool read(FILE* file)
    {
        if (fread(&type, sizeof(FeatureType), 1, file) != sizeof(FeatureType))
            return false;
        // fread returns the number of successfully read elements and not the total size

        short n = 0;
        switch (type)
        {
        case eCircle: n = 3; break;
        case eTriangle: n = 6; break;
        case eSquare: n = 8; break;
        default: type = eUnknown; return false;
            // bad indention
        }
        points = new double[n];
        // no care is taken about the points, if read is called several times there is a leak
        if (!points)
            return false;
        // failed memory allocation throws an exception and this piece of code is not reached
        return fread(&points, sizeof(double), n, file) == n*sizeof(double);
        // fread returns the number of successfully read elements and not the total size
    }
    void draw() // should be const
    {
        switch (type)
        {
        case eCircle: drawCircle(points[0], points[1], points[2]); break;
        case eTriangle: drawPolygon(points, 6); break;
        case eSquare: drawPolygon(points, 8); break;
        // no care is taken about the points, produces run time errors if read was not called before draw or read was unsuccessful
        }
        // using switch in every method of Feature is unconvenient and makes adding new types difficult
    }

protected:
// from the example not clear why protected and not private

    void drawCircle(double centerX, double centerY, double radius); // should take const arguments
    void drawPolygon(double* points, int size); // should take const arguments
    // no connection of draw methods to the class
    // should be rather defined outside of it or at least defined const

    double* points; // much care has to be taken about raw pointers, therefore they are to be avoided
    FeatureType type;
};

int main(int argc, char* argv[])
{
    Feature feature;
    FILE* file = fopen("features.dat", "r");
    // no care is taken for the case when the file is not found
    feature.read(file);
    if (!feature.isValid())
        return 1;
    // fclose(file) should be added
    return 0;
}
