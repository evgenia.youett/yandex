#include <stdio.h>

#include <vector>
#include <memory>
#include <iostream>

// a namespace could be added if the code were to grow
void drawCircle(const double centerX, const double centerY, const double radius) {/*bla*/}
void drawPolygon(const double* points, const int size) {/*bla*/}

std::vector<double> read_vector(FILE* file, short n)
{
    if (n <= 0)
        throw std::runtime_error("number of points must be positive");

    std::vector<double> points(n);
    if (fread(points.data(), sizeof(double), n, file) != n)
        throw std::runtime_error("can't read points from file");

    return points;
}

class Feature
{
public:
    Feature() : points(0) {}

    virtual void draw() const {};
    virtual ~Feature() {};

protected:
    std::vector<double> points;
};

class Circle : public Feature
{
public:
    Circle(std::vector<double>&& points_){
        if(points_.size() != 3) // just to check if some copypasting messed up sizes in the factory when adding new types
            throw std::runtime_error("circle is initialized with wrong number of points");
        this->points = move(points_);
    }

    void draw() const override {
        drawCircle(this->points[0], this->points[1], this->points[2]);}

    virtual ~Circle() {};
};

class Triangle: public Feature
{
public:
    Triangle(std::vector<double>&& points_) {
        if(points_.size() != 6)
            throw std::runtime_error("triangle is initialized with wrong number of points");
        this->points = move(points_);}

    void draw() const override {
        drawPolygon(this->points.data(), 6);}

    virtual ~Triangle() {};
};

class Square : public Feature
{
public:
    Square(std::vector<double>&& points_) {
        if(points_.size() != 8)
            throw std::runtime_error("square is initialized with wrong number of points");
        this->points = move(points_);}

    void draw() const override {
        drawPolygon(this->points.data(), 8);}

    virtual ~Square() {};
};

class FeatureFactory
{
public:
    enum FeatureType {eCircle, eTriangle, eSquare};

    static std::unique_ptr<Feature> makeFeature(FILE* file)
    {
        FeatureType type;
        if (!fread(&type, sizeof(FeatureType), 1, file))
            throw std::runtime_error("can't read feature type from file");
        // right now the code is able to read integers, but it might not be convenient to store the types like this
        // FeatureType can be declared as enum class that prevents implicit casting from int to FeatureType
        // but then read operators for FeatureType must be overloaded

        if(type == eCircle)
            return std::make_unique<Circle>(read_vector(file, 3));
        else if (type == eTriangle)
            return std::make_unique<Triangle>(read_vector(file, 6));
        else if (type == eSquare)
            return std::make_unique<Square>(read_vector(file, 8));
        else
            throw std::runtime_error("feature type not defined");
    }
};
int main(int argc, char* argv[])
{
    try{
        FILE* file = fopen("features.dat", "r");
        if (!file) throw std::runtime_error("file not found");

        auto feature = FeatureFactory::makeFeature(file);

        fclose(file);

    } catch (std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl; }
}
// since the code is rather short, the issue of ceparating declarations from definitions is not considered
